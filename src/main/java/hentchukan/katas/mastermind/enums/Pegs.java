package hentchukan.katas.mastermind.enums;

public enum Pegs {

	BLUE("blue"),
	YELLOW("yellow"),
	BLACK("black"),
	RED("red"),
	WHITE("white"),
	SHIT("shit");
	
	private String label;
	
	Pegs(String label) {
		this.label = label;
	}
	
	public String label() {return this.label;}
}
