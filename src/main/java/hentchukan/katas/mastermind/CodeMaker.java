package hentchukan.katas.mastermind;

import java.util.Arrays;

import hentchukan.katas.mastermind.enums.Pegs;
import hentchukan.katas.mastermind.exceptions.MastermindException;
import hentchukan.katas.mastermind.exceptions.NotSameLengthException;
import hentchukan.katas.mastermind.exceptions.NullArgumentsException;

/**
 * A Main class that implements the CodeMaker Logic for the Mastermind Game.
 * 
 * @author Farooq
 *
 */
public class CodeMaker {

	/**
	 * Main method of the CodeMaker class. 
	 * It evaluates a guess according to certain secret chain of {@link Pegs} 
	 * to determine how many Pegs were well placed guesses, and how many were bad placed guesses.
	 *  
	 * @param guess Pegs sequence suggested by the Code Breaker. 
	 * @param secret Pegs sequence determined by the Code Maker.
	 * @return Two length integer array with <ol>
	 * 	<li>Number of well placed guesses</li>
	 * 	<li>Number of bad placed guesses</li>
	 * </ol>
	 * @throws MastermindException
	 */
	public int[] evaluate(Pegs[] guess, Pegs[] secret) throws MastermindException {
		if (guess == null || secret == null) {
			throw new NullArgumentsException();
		}
		
		if (guess.length != secret.length) {
			throw new NotSameLengthException();
		}
		
		int [] answer = new int[2];
		int i = 0;
		do {
			if (isWellPlaced(guess, secret, i)) {
				answer[0] ++;
				guess = dismiss(guess, i);
				secret = dismiss(secret, i);
				continue;
			} else if (Arrays.asList(secret).contains(guess[i])) {
				answer[1] ++;
			}
			i++;
		} while (i < guess.length);
		
		return answer;
	}

	/**
	 * Removes from a list the item with the given index.
	 * 
	 * @param list Array to be manipulated.
	 * @param index Index of the item to be removed.
	 * @return Array containing <b>list</b> array items but the removed one. The order remains the same.
	 * 
	 * @throws NotSameLengthException if <ul>
	 * <li>The given <b>list</b> should not be null</li>
	 * <li>The index is out of bound</li>
	 * </ul>
	 */
	public Pegs[] dismiss(Pegs[] list, int index) throws NotSameLengthException {
		if (list == null || list.length <= index || index < 0) {
			throw new NotSameLengthException();
		}
		
		Pegs[] result = new Pegs[list.length - 1];
		int i = 0;
		int j = 0;
		while ( i < list.length) {
			if (i != index) {
				result[j] = list[i];
				j++;
			}
			i++;
		}
		return result;
	}

	/**
	 * Checks if the Guessed item with the given index is at the same index in the Secret chain.
	 * 
	 * @param guess CodeBreaker Pegs chain.
	 * @param secret CodeMaker Pegs chain.
	 * @param index Index of the checked item.
	 * @return <ul>
	 * <li><b>True</b> if the items with the index <b>index</b> on both arrays are equals.</li>
	 * <li><b>False</b> otherwise.
	 */
	public boolean isWellPlaced(Pegs[] guess, Pegs[] secret, int index) {
		if (guess == null || secret == null || guess.length != secret.length) {
			return false;
		}
		
		if (index >= guess.length || index < 0) {
			return false;
		} else {
			return guess[index] == secret[index];
		}
	}
	
}
