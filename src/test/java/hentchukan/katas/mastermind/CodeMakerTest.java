/**
 * 
 */
package hentchukan.katas.mastermind;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import hentchukan.katas.mastermind.enums.Pegs;
import hentchukan.katas.mastermind.exceptions.MastermindException;
import hentchukan.katas.mastermind.exceptions.NotSameLengthException;
import hentchukan.katas.mastermind.exceptions.NullArgumentsException;

/**
 * Unit testing the methods in CodeMaker class
 * @author Farooq
 *
 */
public class CodeMakerTest {

	private CodeMaker codemaker;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		codemaker = new CodeMaker();
	}

	@Test(expected = NullArgumentsException.class)
	public void testNullGuessInput() throws MastermindException {
		Pegs [] guess = null;
		Pegs [] secret = {Pegs.BLUE, Pegs.YELLOW};
		
		codemaker.evaluate(guess, secret);
	}
	
	@Test(expected = NullArgumentsException.class)
	public void testNullSecretInput() throws MastermindException {
		Pegs [] guess = {Pegs.BLUE, Pegs.YELLOW};
		Pegs [] secret = null;
		
		codemaker.evaluate(guess, secret);
	}
	
	@Test(expected = NullArgumentsException.class)
	public void testNullInputs() throws MastermindException {
		Pegs [] guess = null;
		Pegs [] secret = null;
		
		codemaker.evaluate(guess, secret);
	}
	
	@Test(expected = NotSameLengthException.class)
	public void testValidInputNumber() throws MastermindException {
		Pegs [] guess = {Pegs.BLACK, Pegs.RED};
		Pegs [] secret = {Pegs.BLUE, Pegs.YELLOW};
		
		assertArrayEquals(new int[] {0, 0}, codemaker.evaluate(guess, secret));

		guess = new Pegs[]{Pegs.BLUE};
		codemaker.evaluate(guess, secret);
	}

	@Test
	public void testIsWellPlaced() {
		Pegs [] guess = {Pegs.BLACK};
		Pegs [] secret = {Pegs.BLACK};
		
		assertTrue(codemaker.isWellPlaced(guess, secret, 0));
		assertFalse(codemaker.isWellPlaced(guess, secret, 1));
		assertFalse(codemaker.isWellPlaced(guess, secret, -1));
		
		guess = new Pegs[] {Pegs.BLUE, Pegs.BLACK, Pegs.RED};
		secret = new Pegs[] {Pegs.YELLOW, Pegs.BLACK, Pegs.BLUE};
		
		assertFalse(codemaker.isWellPlaced(guess, secret, 0));
		assertTrue(codemaker.isWellPlaced(guess, secret, 1));
		assertFalse(codemaker.isWellPlaced(guess, secret, 2));
		
		guess = new Pegs[] {Pegs.BLUE, Pegs.BLACK, Pegs.RED, Pegs.RED};
		secret = new Pegs[] {Pegs.YELLOW, Pegs.BLUE, Pegs.BLUE, Pegs.RED};
		
		assertFalse(codemaker.isWellPlaced(guess, secret, 0));
		assertFalse(codemaker.isWellPlaced(guess, secret, 1));
		assertFalse(codemaker.isWellPlaced(guess, secret, 2));
		assertTrue(codemaker.isWellPlaced(guess, secret, 3));
	}
	
	@Test
	public void testEvaluate_OneSizedGuess() throws MastermindException {
		Pegs [] guess = {Pegs.BLACK};
		Pegs [] secret = {Pegs.BLACK};
		
		codemaker = Mockito.spy(CodeMaker.class);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 0)).thenReturn(true);
		
		int[] answer = codemaker.evaluate(guess, secret);
		
		assertEquals(2, answer.length);
		assertEquals(1, answer[0]);
		assertEquals(0, answer[1]);
	}
	
	@Test
	public void testEvaluate_IdenticalPegs() throws MastermindException {
		Pegs [] guess = {Pegs.BLACK, Pegs.BLACK, Pegs.BLACK};
		Pegs [] secret = {Pegs.BLACK, Pegs.BLACK, Pegs.BLACK};
		
		codemaker = Mockito.spy(CodeMaker.class);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 0)).thenReturn(true);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 1)).thenReturn(true);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 2)).thenReturn(true);
		
		int[] answer = codemaker.evaluate(guess, secret);
		
		assertEquals(2, answer.length);
		assertEquals(3, answer[0]);
		assertEquals(0, answer[1]);
	}
	
	@Test
	public void testEvaluate_DuplicatePlacedPeg() throws MastermindException {
		Pegs [] guess = {Pegs.BLACK, Pegs.RED, Pegs.BLUE};
		Pegs [] secret = {Pegs.BLUE, Pegs.RED, Pegs.RED};
		
		codemaker = Mockito.spy(CodeMaker.class);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 0)).thenReturn(false);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 1)).thenReturn(true);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 2)).thenReturn(false);
		
		int[] answer = codemaker.evaluate(guess, secret);
		
		assertEquals(2, answer.length);
		assertEquals(1, answer[0]);
		assertEquals(1, answer[1]);
	}
	
	@Test
	public void testEvaluate_DuplicateGuessedPeg() throws MastermindException {
		Pegs [] guess = {Pegs.BLACK, Pegs.RED, Pegs.RED};
		Pegs [] secret = {Pegs.BLUE, Pegs.RED, Pegs.BLACK};
		
		codemaker = Mockito.spy(CodeMaker.class);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 0)).thenReturn(false);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 1)).thenReturn(true);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 2)).thenReturn(false);
		
		int[] answer = codemaker.evaluate(guess, secret);
		
		assertEquals(2, answer.length);
		assertEquals(1, answer[0]);
		assertEquals(1, answer[1]);
	}
	
	@Test
	public void testEvaluate_DoubleDuplicateCase() throws MastermindException {
		Pegs [] guess = {Pegs.RED, Pegs.RED, Pegs.RED, Pegs.BLACK};
		Pegs [] secret = {Pegs.RED, Pegs.RED, Pegs.BLACK, Pegs.BLACK};
		
		codemaker = Mockito.spy(CodeMaker.class);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 0)).thenReturn(true);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 1)).thenReturn(true);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 2)).thenReturn(false);
		Mockito.when(codemaker.isWellPlaced(guess, secret, 2)).thenReturn(true);
		
		int[] answer = codemaker.evaluate(guess, secret);
		
		assertEquals(2, answer.length);
		assertEquals(3, answer[0]);
		assertEquals(0, answer[1]);
	}
	
	@Test(expected = NotSameLengthException.class)
	public void testDismiss_NullList() throws MastermindException {
		codemaker.dismiss(null, 0);
	}
	
	@Test(expected = NotSameLengthException.class)
	public void testDismiss_IndexOutOfBound() throws MastermindException {
		Pegs [] list = {Pegs.BLACK, Pegs.BLUE};
		codemaker.dismiss(list, 2);
	}
	
	@Test(expected = NotSameLengthException.class)
	public void testDismiss_Empty() throws MastermindException {
		Pegs [] list = {};
		codemaker.dismiss(list, 0);
	}
	
	@Test
	public void testDismiss_GoodOnes() throws NotSameLengthException {
		Pegs [] list = {Pegs.BLACK};
		Pegs[] result = codemaker.dismiss(list, 0);
		assertArrayEquals(new Pegs[] {}, result);
		
		list = new Pegs[] {Pegs.BLACK, Pegs.RED, Pegs.BLUE};
		result = codemaker.dismiss(list, 2);
		assertArrayEquals(new Pegs[] {Pegs.BLACK, Pegs.RED}, result);
		
		result = codemaker.dismiss(list, 1);
		assertArrayEquals(new Pegs[] {Pegs.BLACK, Pegs.BLUE}, result);
		
		result = codemaker.dismiss(list, 0);
		assertArrayEquals(new Pegs[] {Pegs.RED, Pegs.BLUE}, result);
	}
	
}
